// variables
var themeDir =  "";
var gulp = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var browserSync = require('browser-sync');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');
var imagemin = require("gulp-imagemin");
var fs = require('fs');
var path = require("path");
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");
var autoprefixer = require("gulp-autoprefixer");


//config
var root = "assets/",
config = {
   "path" : {
      "htdocs"    : root,
      "sass"      : root + "scss",
      "css"       : root + "css",
	  "img"       : root + "_images",
	  "min_img"   : root + "images",
      "js"        : root + "_js",
      "min_js"    : root + "js",
   }
};


//画像をミニファイド
gulp.task('imageMinTask', function() { // 「imageMinTask」という名前のタスクを登録
    gulp.src(config.path.img + '/**/*.{png,jpg,svg,webp,gif}' )    // imagesフォルダー以下のpng画像を取得
        .pipe(imagemin())   // 画像の圧縮処理を実行
        .pipe(gulp.dest(config.path.min_img));   // minified_imagesフォルダー以下に保存
});

// JSをミニファイド
gulp.task('js.concat', function() {
	gulp.src( config.path.js + "/*.js")
		.pipe(plumber())
		.pipe(uglify())
		.pipe(gulp.dest(config.path.min_js));
	gulp.src(config.path.js + "/_vendor/*.js")
		.pipe(plumber())
		.pipe(concat('plugins.js'))
		.pipe(uglify({output: {comments: 'some'}}))
		.pipe(gulp.dest(config.path.min_js));
});
//ブラウザを自動で同期
gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: themeDir // ルートとなるディレクトリを指定
        }
    });
});

gulp.task('bs-reload', function () {
    browserSync.reload();
});

//ベンダープレフィックス設定
gulp.task('auto-prefix-with-scss', function () {
    return gulp.src(config.path.sass + '/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sass())
        .pipe(cssmin())
        .pipe(autoprefixer({
            // ☆IEは9以上、Androidは4以上、iOS Safariは8以上
            // その他は最新2バージョンで必要なベンダープレフィックスを付与する設定
            browsers: ["last 2 versions", "ie >= 9", "Android >= 4","ios_saf >= 8"],
            cascade: false
        }))
        .pipe(gulp.dest(config.path.css))
        .pipe(browserSync.reload({stream: true}));
});

//監視
gulp.task('watch', function () {

	watch(config.path.sass+'/**/*.scss', function(arg){
        if(arg.type=="deleted"){
            var cssFilePath = arg.path.replace(config.path.sass,config.path.css).replace("scss","css");
            if(path.existsSync(cssFilePath)){
                fs.unlink(cssFilePath);
            }
        }else{
            gulp.start('auto-prefix-with-scss');
        }
    });

	watch(themeDir + "*.html", function(){
		gulp.start(['bs-reload']);
	});

	watch(config.path.img + "/**/*.{png,jpg,svg,webp,gif}", function(){
		gulp.start(['imageMinTask']);
	});

	watch(config.path.js + "/**/*.js", function () {
		gulp.start(['js.concat']);
	});

});

gulp.task('default', [
	'browser-sync',
	'watch'
]);