$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 400; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});


$(window).on('load', function() { 
    var gMap = new GMap('.map-area'); 
});

//
var GMap = function(el) {
    this.init(el);
    this.handleEvents();
};

GMap.prototype.init = function(el) {
    this.$el = $(el);
    this.$el.children('iframe').css('pointer-events','none');
};

GMap.prototype.handleEvents = function() {
    var self = this;
    this.$el.on('click', function(e) {
        $(e.currentTarget).children('iframe').css('pointer-events','auto');
    });
    this.$el.on('mouseleave', function(e) {
        $(e.currentTarget).children('iframe').css('pointer-events','none');
    });
};

